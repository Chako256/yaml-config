const yaml = require('js-yaml')
const fs = require('fs')

class Configurator {
    constructor(filePath) {
        let cfg = yaml.safeLoad(fs.readFileSync(filePath))
        this.data = Object.assign({}, cfg.global, cfg[process.env.NODE_ENV])
    }

    get(key) {
        let keys = key.split('.')
        let result = this.data
        for (let key of keys) {
            result = result[key]
        }
        return result
    }
}
module.exports = Configurator
