# Configurator for NodeJS applications

This tiny module is useful to easily fetch configurations for multiple environments.

## Installation

```
$ npm install --save https://bitbucket.org/Chako256/yaml-config.git
```

## Usage

```javascript
import { Configurator } from 'yaml-config'

let config = new Configurator('config/config.yml')
config.get('value')
```

## YAML Usage

```yaml
# Yaml Example
global:
    key1: value
    key2:
        key2A: value2
        key2B: value3
env1:
    key3: value4
    key4: value5
    key5: value6
env2:
    key2:
        key2A: value0
```

You can override global values inside environments values.
Be aware overriding an object value will overwrite the entire object and not only the given properties.

In the above example, the `key2` value for `env2` will not have a `key2B` key.

## Copyright

Copyright (C) 2016 Michael Chacaton. All rights reserved. 
